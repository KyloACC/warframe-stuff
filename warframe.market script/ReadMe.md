# Warframe market full statistics
This is a program written in python for the purpose of scraping warframe.market for statistics on the listed items. This will generate a .json file, a pseudo-table in the format of a .txt, as well as a .csv file for easy import to a spreadsheet.

## Setup
Install python if you haven't before:

https://www.python.org/ 

Install the requirements.txt by going into command prompt and typing:
```
pip install requirements.txt
```

## Usage
To run the first time:
python waframe_market_full_statistics.py

This will take a while, but will let you know it's progress as it goes.

After the first run, you can run it again using:

```
python waframe_market_full_statistics.py  -f "C:/Python Scripts/All_Statistics.json"
```

("C:/Python_Scripts/All_Statistics.json" is just an example)

You can sort the pandas table using: -s "48h closed MA" "48h closed AP"

You can load that statistics json, and update specific items instead of the whole 3000+ list of them using: -u "Ammo Drum" "Axi H2 Intact" "Aklex Prime Blueprint"

You can also get the same information on -f, -s, and -u by doing:
python waframe_market_full_statistics.py -h