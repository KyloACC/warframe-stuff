from pathlib import Path
import argparse
import datetime
import time
import json
import operator
import sys

import requests
import pandas
from tabulate import tabulate
import openpyxl

"""
Author: Jonathan Sourdough
Email: Jonathan@JSourdough.com
version: 1.3
Date: 6-6-2020
License: GPL v2.0
"""

if __name__ == "__main__":
    # [name on table, buy/sell/closed, statistic type, 48hours/90days]
    sort_by = [
        "Items Names",
    ]
    wanted = [
        ["48h Closed MA", "closed", "moving_avg", "48hours"],
        ["48h Closed AP", "closed", "avg_price", "48hours"],
        ["90d Closed MA", "closed", "moving_avg", "90days"],
        ["90d Closed AP", "closed", "avg_price", "90days"],
        ["48h Sell MA", "closed", "moving_avg", "48hours"],
        ["48h Sell AP", "sell", "avg_price", "48hours"],
        ["48h Buy MA", "buy", "moving_avg", "48hours"],
        ["48h Buy AP", "buy", "avg_price", "48hours"],
        ["90d Sell MA", "sell", "moving_avg", "90days"],
        ["90d Sell AP", "sell", "avg_price", "90days"],
        ["90d Buy MA", "buy", "moving_avg", "90days"],
        ["90d Buy AP", "buy", "avg_price", "90days"],
    ]


def timestamp():
    return datetime.datetime.isoformat(datetime.datetime.now())


def name_check(wfm_api, update_names=[]):
    raw_items = requests.get(wfm_api + "items").json()["payload"]["items"]
    items = []
    for raw_item in raw_items:
        if not update_names == []:
            if raw_item["item_name"] not in update_names:
                continue
        items.append(
            {"item_name": raw_item["item_name"], "url_name": raw_item["url_name"],}
        )
    return items


def try_add(log, statistics, info):
    if info[0] not in statistics:
        statistics[info[0]] = 0
    elif statistics[info[0]] != 0:
        return statistics
    if info[1] == "closed":
        pass
    elif info[1] == log["order_type"]:
        pass
    else:
        return statistics
    try:
        statistics[info[0]] = log[info[2]]
    except KeyError:
        pass
    return statistics


def loop_through_log(statistics_raw, wanted):
    temp = {}
    for info in wanted:
        if info[1] == "closed":
            endpoint = statistics_raw["statistics_closed"][info[3]]
        else:
            endpoint = statistics_raw["statistics_live"][info[3]]
        if not endpoint == []:
            endpoint.reverse()
        for log in endpoint:
            temp = try_add(log, temp, info)
            if not 0 in temp.values():
                break
    return temp


def get_item_statistics(wfm_api, wanted, items, index):
    this_item = items[index]
    statistics_raw = requests.get(
        wfm_api + "/items/" + this_item["url_name"] + "/statistics"
    ).json()["payload"]

    statistics = {}

    for info in wanted:
        statistics[info[0]] = 0

    statistics.update(loop_through_log(statistics_raw, wanted))

    this_item["statistics"] = statistics

    return items


def statistics_check(wfm_api, wanted, items):
    for i in range(len(items)):
        sys.stdout.write("\r")
        sys.stdout.flush()
        sys.stdout.write("[{}/{}]".format(i + 1, len(items)))
        sys.stdout.flush()
        # print("[{}/{}]".format(i + 1, len(items)))
        start_time = time.time()
        items = get_item_statistics(wfm_api, wanted, items, i)
        if (time.time() - start_time) >= 0.34:
            continue
        sleep = 0.34 - (time.time() - start_time)
        if sleep < 0.01:
            sleep = 0.01
        time.sleep(sleep)
    return items


def loadJson(file):
    if Path(file).is_file():
        with open(file, "r") as f:
            try:
                return json.loads(f.read())
            except json.decoder.JSONDecodeError:
                print("incorrect json format")
                quit()
            except FileNotFoundError:
                print("Path to file incorrect")


def make_panda_dataframe(items):
    df_dict = {
        "Items Names": [],
    }
    for item in items:
        df_dict["Items Names"].append(item["item_name"])
        for statistic_name, statistic_value in item["statistics"].items():
            if statistic_name not in df_dict:
                df_dict[statistic_name] = []
            df_dict[statistic_name].append(statistic_value)
    return df_dict


if __name__ == "__main__":
    scriptDir = Path(__file__).resolve().parent
    wfm_api = "https://api.warframe.market/v1/"

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f",
        "--file",
        help="full path to a .json file with statistics to use instead of retrieving new data",
        required=False,
    )
    parser.add_argument(
        "-u",
        "--update",
        nargs="+",
        help="list of names of items you want updated. Format: "
        + """-u 'Ammo Drum' 'Axi H2 Intact' 'Aklex Prime Blueprint'""",
        required=False,
    )
    parser.add_argument(
        "-s",
        "--sort",
        nargs="+",
        help="which columns you want to sort by (Supports nested sorting). Format: "
        + """-s '48h closed MA' '48h closed AP'""",
        required=False,
    )

    args = vars(parser.parse_args())

    if not args["file"] == None:
        print("loading .json file")
        items = loadJson(args["file"])
    else:
        print("Started name import", timestamp())
        items = name_check(wfm_api)
        print("Started statistics import", timestamp())
        items = statistics_check(wfm_api, wanted, items)

    if not args["update"] == None:
        if args["file"] == None:
            print('"--update" also requires "--file"')
            exit()

        print("Started name import", timestamp())
        update_items = name_check(wfm_api, args["update"])
        print("Started statistics import", timestamp())
        update_items = statistics_check(wfm_api, wanted, update_items)

        for i, item in enumerate(items):
            for updated_item in list(update_items):
                if item["item_name"] == updated_item["item_name"]:
                    items[i] = updated_item
                    update_items.remove(updated_item)
        if not update_items == []:
            items.extend(update_items)

    if not args["sort"] == None:
        try:
            sort_by = eval(args["sort"])
        except NameError:
            print("check your formatting for --sort")
    (scriptDir / "All_Statistics.json").write_text(json.dumps(items, indent=4))
    print("Json file saved.")

    print("Started compiling dataframe for pandas", timestamp())
    df_dict = make_panda_dataframe(items)
    dataframe = pandas.DataFrame(df_dict)
    dataframe_sorted = dataframe.sort_values(by=sort_by, axis=0, ascending=False)
    dataframe_text = tabulate(
        dataframe_sorted,
        showindex=False,
        headers=dataframe_sorted.columns,
        tablefmt="fancy_grid",
        numalign="left",
    )
    (scriptDir / "Statistics_Panda.txt").write_text(dataframe_text, encoding="utf-8")
    print("Pandas file saved.")
    (scriptDir / "Statistics.csv").write_text(dataframe_sorted.to_csv(), encoding="utf-8")
    dataframe_sorted.to_excel(str((scriptDir / "Statistics.xlsx")))
    print("CSV file saved.")
